<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function index()
    {
        return view ('admin.home.index');
    }

    public function emprestimos(){

       return view ('admin.emprestimos.index');
    }

    // public function linhas(){
    //     return view ('admin.linhas.index');
    // }

    public function aparelhos(){
        return view ('admin.aparelhos.index');
    }
}
