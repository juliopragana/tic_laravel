<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Linha;
use Exception;
use Illuminate\Support\Facades\DB;

class AparelhoController extends Controller
{
    // Rota principal
    public function index(Request $request){ 
        return view ('admin.aparelhos.index');
    }

    // Método que carrega os dados do dashboard
    public function get_ind(Request $request){
        //conta todos os registros da tabela
        $total = DB::table('aparelhos')->count();
        //conta os registros das linhas disponíveis
        $totalD = DB::table('aparelhos')->where('status_aparelho','=',0)->count();
        //conta os registros das linhas ocupadas
        $totalO = DB::table('aparelhos')->where('status_aparelho','=',1)->count();
        //conta os registros das linhas com pendencias
        $totalP = DB::table('aparelhos')->where('status_aparelho','=',2)->count();

		$res = array(
			'total' => $total,
			'aparelhos_d' => $totalD,
			'aparelhos_o' => $totalO,
			'aparelhos_p' => $totalP
		);
		
		return $res;
    }
}
