<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Linha;
use App\models\VincLinha;
use App\models\Funcionario;
use Exception;
use Illuminate\Support\Facades\DB;

class LinhaController extends Controller
{
    // // 
    // public function index(Request $request){

    //     //conta todos os registros da tabela
    //     $total = DB::table('linhas')->count();
    //     //conta os registros das linhas disponíveis
    //     $totalD = DB::table('linhas')->where('status_linha','=',0)->count();
    //     //conta os registros das linhas ocupadas
    //     $totalU = DB::table('linhas')->where('status_linha','=',1)->count();
    //     //conta os registros das linhas com pendencias
    //     $totalP = DB::table('linhas')->where('status_linha','=',2)->count();
    //     //trás todos os nomes dos funcionários
    //     $funcs = Funcionario::orderBy('nome', 'asc')->get();
    //     //trás somente as linhas disponíveis
    //     $linhasD = DB::table('linhas')->where('status_linha', '=', 0)->get();
    //     //trás os registros da tabela vinc_linhas
    //     $linhasV = DB::table('vinc_linhas')
    //                     ->join('funcionarios','vinc_linhas.func_id', '=', 'funcionarios.id')
    //                     ->join('linhas','vinc_linhas.linha_id', '=', 'linhas.id')
    //                     ->select('vinc_linhas.*', 'funcionarios.*', 'linhas.*')->get();
    //     //trás os registros da tabela Linhas       
    //     $linhas = Linha::orderBy('status_linha', 'desc')->get();
        
    //     return view ('admin.linhas.home', compact('linhas', 'linhasV', 'total', 'totalD', 'totalU', 'totalP', 'funcs', 'linhasD'));
        
    // }


     // 
     public function index(Request $request){

        //conta todos os registros da tabela
        $total = DB::table('linhas')->count();
        //conta os registros das linhas disponíveis
        $totalD = DB::table('linhas')->where('status_linha','=',0)->count();
        //conta os registros das linhas ocupadas
        $totalU = DB::table('linhas')->where('status_linha','=',1)->count();
        //conta os registros das linhas com pendencias
        $totalP = DB::table('linhas')->where('status_linha','=',2)->count();
        //trás todos os nomes dos funcionários
        $funcs = Funcionario::orderBy('nome', 'asc')->get();
        //trás somente as linhas disponíveis
        // $linhasD = DB::table('linhas')->where('status_linha', '=', 0)->get();
        //trás os registros da tabela vinc_linhas
        
        // $linhasV = DB::table('linhas')
        //                 ->leftJoin('vinc_linhas','linhas.id', '=', 'vinc_linhas.id')
        //                 ->leftJoin('funcionarios','vinc_linhas.func_id', '=', 'funcionarios.id')
        //                 ->select('linhas.id','linhas.num_linha','linhas.estado','linhas.status_linha', DB::raw('(CASE WHEN funcionarios.nome =  null  THEN "sem dono" ELSE funcionarios.nome END) AS nome'))
        //                 ->get();

        $linhasD = collect(DB::select(DB::raw('SELECT 
                l.id, l.num_linha, l.estado, l.status_linha, l.observacao,
                f.nome, f.filial, f.setor, f.cargo, f.centro_custo, f.num_centro_custo
                    FROM  linhas l
                    LEFT JOIN vinc_linhas  vl ON (l.id = vl.linha_id)
                    LEFT JOIN funcionarios f ON (vl.func_id = f.id) 
                            WHERE l.status_linha = 0
                ') 
        ));

        $linhasO = collect(DB::select(DB::raw('SELECT 
                l.id, l.num_linha, l.estado, l.status_linha, l.observacao,
                f.nome, f.filial, f.setor, f.cargo, f.centro_custo, f.num_centro_custo
                    FROM  linhas l
                    LEFT JOIN vinc_linhas  vl ON (l.id = vl.linha_id)
                    LEFT JOIN funcionarios f ON (vl.func_id = f.id) 
                    WHERE l.status_linha = 1
                ') 
        ));
                    
        // 
        // $liv = json_encode($lv);

                   
        //trás os registros da tabela Linhas       
        $linhas = Linha::orderBy('status_linha', 'desc')->get();
        
                  
        return view ('admin.linhas.home', ['linhasD'=> $linhasD, 'linhasO'=> $linhasO, 'total'=> $total, 'totalD' => $totalD, 'totalU' => $totalU, 'totalP'=> $totalP, 'funcs' => $funcs, 'linhasD'=> $linhasD ]);
    }

    public function get_ind(Request $request){
        //conta todos os registros da tabela
        $total = DB::table('linhas')->count();
        //conta os registros das linhas disponíveis
        $totalD = DB::table('linhas')->where('status_linha','=',0)->count();
        //conta os registros das linhas ocupadas
        $totalO = DB::table('linhas')->where('status_linha','=',1)->count();
        //conta os registros das linhas com pendencias
        $totalP = DB::table('linhas')->where('status_linha','=',2)->count();

		$res = array(
			'total' => $total,
			'linhas_d' => $totalD,
			'linhas_o' => $totalO,
			'linhas_p' => $totalP
		);
		
		return $res;
    }
	
    public function carregaTablesdisp(Request $request){

        $result = [];

        $linhasD = collect(DB::select(DB::raw('SELECT 
            l.id, l.num_linha, l.estado, l.status_linha, l.observacao,
            f.nome, f.filial, f.setor, f.cargo, f.centro_custo, f.num_centro_custo, 
            (CASE WHEN l.status_linha = 0 THEN "Disponível" ELSE "" END) as st_linha
                FROM  linhas l
                LEFT JOIN vinc_linhas  vl ON (l.id = vl.linha_id)
                LEFT JOIN funcionarios f ON (vl.func_id = f.id) 
                        WHERE l.status_linha = 0
            ')
        ));


        foreach($linhasD as $key => $value){
            array_push($result, $value);
        }

        return $result;

    }

    public function carregaTablesocup(Request $request){

        $result = [];

        $linhasO = collect(DB::select(DB::raw('SELECT 
            l.id, l.num_linha, l.estado, l.status_linha, l.observacao,
            f.nome, f.filial, f.setor, f.cargo, f.centro_custo, f.num_centro_custo, 
            (CASE WHEN l.status_linha = 1 THEN "Em uso" ELSE "" END) as st_linha
                FROM  linhas l
                LEFT JOIN vinc_linhas  vl ON (l.id = vl.linha_id)
                LEFT JOIN funcionarios f ON (vl.func_id = f.id) 
                        WHERE l.status_linha = 1
            ')
        ));

        foreach($linhasO as $key => $value){
            array_push($result, $value);
        }

        return $result;
        // return json_encode($linhasO);

    }


    public function salvar(Request $request){

        $func = $request->nome;
        $num_linha = $request->telefone;
        $estado = $request->estado;
        $observacao = $request->observacao;


        if($func){

            $linha = new Linha;
            $linha->num_linha = $num_linha;
            $linha->estado = $estado;
            $linha->status_linha = 1;
            $linha->observacao = $observacao;
            $linha->save();
           
            $vincFunc = new VincLinha;
            $vincFunc->func_id = $func;
            $vincFunc->linha_id = $linha->id;
            $vincFunc->save();

            return response()->json(['success'=> 'Cadastro realizado com sucesso!!']);
           
        } else {

            $linha = new Linha;
            $linha->num_linha = $num_linha;
            $linha->estado = $estado;
            $linha->status_linha = 0;
            $linha->observacao = $observacao;

            $linha->save();
           
            
            return response()->json(['success'=> 'Cadastro e vinculação realizados com sucesso!!']);
            
        }

        
    }

    public function info(Request $request){

        $id = $request->id;

        $linha = Linha::find($id);
        

        if($linha->status_linha == 1){
            
            $linhasV = DB::table('vinc_linhas')
            ->join('funcionarios','vinc_linhas.func_id', '=', 'funcionarios.id')
            ->join('linhas','vinc_linhas.linha_id', '=', 'linhas.id')
            ->where('linhas.id', '=', $request->id)->get();
            
            $dados = (object)$linhasV;
            return $dados;

        } if ($linha->status_linha == 0){
            return $linha->toArray();
        }
        return $id;

    }

    public function modal_edit(Request $request){

        $id = $request->id; 
        $linha = Linha::find($id);
        

        if($linha->status_linha == 1){
            
            $linhasV = DB::table('vinc_linhas')
            ->join('funcionarios','vinc_linhas.func_id', '=', 'funcionarios.id')
            ->join('linhas','vinc_linhas.linha_id', '=', 'linhas.id')
            ->where('linhas.id', '=', $request->id)->get();
            
            $dados = (object)$linhasV;
            return $dados;

        } if ($linha->status_linha == 0){
            return $linha->toArray();
        }
        return $id;
    }



    public function editar(Request $request){

        // $id_ = $request->id;
        // $id_linha = $request->id_linha;
        $id_linha = 0;
        if($request->id != null){
            $id_linha = $request->id;
        } else if($request->id_linha != null){
            $id_linha = $request->id_linha;
        }
        $id_func = $request->id_func;
        $prop = $request->prop;
        $num_linha = $request->telefone;
        $estado = $request->estado;
        $observacao = $request->observacao;



        if($prop != null){

            $linha = Linha::find($id_linha);
            $linha->num_linha = $num_linha;
            $linha->estado = $estado;
            $linha->status_linha = 1;
            $linha->observacao = $observacao;
            $linha->save();
           
            
          
            // $vincFunc = VincLinha::where('linha_id', $id_linha)->get();
            $vincFunc = VincLinha::where('linha_id', $id_linha)->get();

            if(isset($vincFunc[0])){
                // $vincFunc = DB::table('vinc_linhas')->where('linha_id', '=', $id_linha)->get();
                $vincFunc[0]->func_id = $prop;
                $vincFunc[0]->linha_id = $id_linha;
                $vincFunc[0]->save();
                
                
            }else {
                $vincFunc = new VincLinha;
                $vincFunc->func_id = $prop;
                $vincFunc->linha_id = $id_linha;
                $vincFunc->save();
            }
            

            return response()->json(['success'=> 'Linha alterada com sucesso!!']);
           
        } else {

            $linha = Linha::find($id_linha);
            $linha->num_linha = $num_linha;
            $linha->estado = $estado;
            $linha->status_linha = 0;
            $linha->observacao = $observacao;

            $linha->save();
           
            
            return response()->json(['success'=> 'Linha alterada com sucesso!!']);
            // return response()->json(['success'=> $linha]);
            
        }

        
        // return response()->json(['success' );
        
    }

    public function desv_linha(Request $request){
        $id_linha = $request->id_linha;

        $vincLinha = VincLinha::where('linha_id', $id_linha)->get();
        $vincLinha[0]->delete();

        $linha = Linha::where('id', $id_linha)->get();
        $linha[0]->status_linha = 0;
        $linha[0]->save();

        return response()->json(['success'=> 'Linha alterada com sucesso!!']);
    }


    public function debug() {
       
    }

    public function deleta_linha(Request $request){
        $id = $request->id_linha;

        try{

            DB::table('linhas')->where('id', '=', $id)->delete();
            return response()->json(['success'=> 'Linha deletada com sucesso!!']);

        }catch(Exception $e){
            return response()->json(['error'=> 'Houve um erro ao tentar excluir a linha. Tente mais tarde, se o problema persistir, favor procure o administrador do sistema.']);
        }   
        
    }

    
}
