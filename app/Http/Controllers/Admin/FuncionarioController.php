<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Funcionario;

use Exception;
use Illuminate\Support\Facades\DB;



class FuncionarioController extends Controller
{
    //

    public function funcionarios(Request $request){
        $funcs = DB::table('funcionarios')->orderBy('nome', 'asc')->get();

        return $funcs;
    }

}
