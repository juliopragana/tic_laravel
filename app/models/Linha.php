<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
    protected $fillable= ['num_linha', 'estado', 'status_linha', 'observacao'];

}
