$(function () {

  //função que oculta ou exibe o funcionário no cadastro da linha
  $('[name="check"]').change(function () {
    $('#minhaDiv').toggle(100);
  });


  //Select2
  $('.select2').select2()
  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

  // Função do CSRF - Larável
  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  //Máscara de telefone
  jQuery("input.telefone")
    .mask("(99) 9999-9999?9")
    .focusout(function (event) {
      var target, phone, element;
      target = (event.currentTarget) ? event.currentTarget : event.srcElement;
      phone = target.value.replace(/\D/g, '');
      element = $(target);
      element.unmask();
      if (phone.length > 10) {
        element.mask("(99) 99999-999?9");
      } else {
        element.mask("(99) 9999-9999?9");
      }
    });


  //função que carrega a DataTable de linhas disponíveis
  var table = $('#table_disp').DataTable({
    "ajax": {
      "url": "/admin/linhas/carrega_tabela_disp",
      "dataSrc": ""
    },
    "bProcessing": true,
    "columns": [

      { "data": "num_linha", "className": "text-center" },
      { "data": "estado", "className": "text-center" },
      { "data": "st_linha", "className": "text-center" },
      { "data": "observacao", "className": "text-center" },
      {
        "data": "id", "className": "text-center", render: function (data, type, row, meta) {
          return type === 'display' ?
            '<button class="btn btn-info btn-sm visualizar" onclick="vizualizar(' + data + ')" data-id="' + data + '"><i class="fas fa-eye"></i></button>\
                  <button class="btn btn-warning btn-sm modalEdicao" onclick="editar('+ data + ')" data-id="' + data + '"><i class="fas fa-edit"></i></button>\
                  <button class="btn btn-danger btn-sm" onclick="deletar('+ data + ')" data-id="' + data + '"><i class="fas fa-trash-alt"></i></button>\
                  '
            :
            data;
        }
      },
    ],
    "language": {
      "url": "/vendor/datatables/dataTables-pt_br.json"
    }
  });

  //função que carrega a DataTable de linhas ocupadas
  var table = $('#table_ocup').DataTable({
    "ajax": {
      "url": "/admin/linhas/carrega_tabela_ocup",
      "dataSrc": ""
    },
    "bProcessing": true,
    "columns": [

      { "data": "num_linha", "className": "text-center" },
      { "data": "estado", "className": "text-center" },
      { "data": "st_linha", "className": "text-center" },
      { "data": "observacao", "className": "text-center" },
      { "data": "nome", "className": "text-center" },
      {
        "data": "id", "className": "text-center", render: function (data, type, row, meta) {
          return type === 'display' ?
            '<button class="btn btn-info btn-sm visualizar" onclick="vizualizar(' + data + ')" data-id="' + data + '"><i class="fas fa-eye"></i></button>\
                  <button class="btn btn-warning btn-sm modalEdicao" onclick="editar('+ data + ')" data-id="' + data + '"><i class="fas fa-edit"></i></button>'
            :
            data;
        }
      },
    ],
    "language": {
      "url": "/vendor/datatables/dataTables-pt_br.json"
    }
  });
});

// função que abre o modal de vizualização das linhas
function vizualizar(id) {


  console.log(id);
  $.ajax({

    type: 'POST',

    url: '/admin/linhas/infor_linha',

    data: { id: id },

    success: function (data) {

      var div = document.getElementById('divInfoLinhas');

      if (Array.isArray(data)) {

        console.log(data[0]['nome']);
        var status_linha = "";
        var obs = "";

        if (data[0]['status_linha'] == 0) {
          var status_linha = "Disponível";
        }
        if (data[0]['status_linha'] == 1) {
          var status_linha = "Ocupada";
        }

        if (!!data[0]['observacao']) {
          var obs = data[0]['observacao'];
        }

        div.innerHTML =
          "<h4 style='color:#00004d'>Dados da Linha</h4><br>\
          <p> <b>Linha:</b> <span class='dados'>" + data[0]['num_linha'] + "</span></p>\
          <p> <b>Estado:</b> <span class='dados'>" + data[0]['estado'] + "</span></p>\
          <p> <b>Status:</b> <span class='dados'>" + status_linha + "</span></p>\
          <p> <b>Observação:</b> <span class='dados'>" + obs + "</span></p><hr>\
          <h4 style='color:#00004d'> Dados do proprietário </h4><br>\
          <p> <b>Nome:</b> <span class='dados'>"+ data[0]['nome'] + "</span></p>\
          <p> <b>Filial:</b> <span class='dados'>"+ data[0]['filial'] + "</span></p>\
          <p> <b>Setor:</b> <span class='dados'>"+ data[0]['setor'] + "</span></p>\
          <p> <b>Cargo:</b> <span class='dados'>"+ data[0]['cargo'] + "</span></p>\
          <p> <b>Centro de Custo:</b> <span class='dados'>"+ data[0]['centro_custo'] + "</span></p>\
          <p> <b>Número do C.C:</b> <span class='dados'>"+ data[0]['num_centro_custo'] + "</span></p>\
          ";

      }
      else {

        var status_linha = "";
        var obs = "";

        if (data.status_linha == 0) {
          var status_linha = "Disponível";
        }
        if (data.status_linha == 1) {
          var status_linha = "Ocupada";
        }

        var numero = data.num_linha;
        var estado = data.estado;

        if (!!data.observacao) {
          var obs = data.observacao;
        }

        div.innerHTML = "<h4 style='color:#00004d'>Dados da Linha</h4><br>\
          <p> <b>Linha:</b> <span class='dados'>" + numero + "</span></p>\
          <p> <b>Estado:</b> <span class='dados'>" + estado + "</span></p>\
          <p> <b>Status:</b> <span class='dados'>" + status_linha + "</span></p>\
          <p> <b>Observação:</b> <span class='dados'>" + obs + "</span></p><hr>";

      }

      $("#infoLinha").modal();


      console.log(data);


    }

  });

};

//função que abre o modal de edição das linhas
function editar(id) {

  console.log(id);
  $.ajax({

    type: 'POST',

    url: '/admin/linhas/modal_edit',

    data: { id: id },

    success: function (data) {

      var div = document.getElementById('editarL');

      if (Array.isArray(data)) {

        console.log(data[0]['nome']);
        var status_linha = "";

        if (data[0]['status_linha'] == 0) {
          var status_linha = "Disponível";
        }
        if (data[0]['status_linha'] == 1) {
          var status_linha = "Ocupada";
        }

        var obs = "";

        if (!!data[0]['observacao']) {
          var obs = data[0]['observacao'];
        }

        div.innerHTML =
          '<div class="form-group">\
                    <input type="hidden" id="func_id" value="'+ data[0]['func_id'] + '">\
                    <input type="hidden" id="linha_id" value="'+ data[0]['linha_id'] + '">\
                    </div>\
                    <div class="form-group">\
                      <label for="estado">Número do telefone</label>\
                      <input type="text" class="form-control telefone" name="tel" id="tel" aria-describedby="numeroFone" value="'+ data[0]['num_linha'] + '">\
                     </div>\
                     <div class="form-group">\
                      <label for="estado">Estado</label>\
                      <select class="form-control select2bs4 select2bs4-danger" style="width: 100%;" id="est" name="est">\
                            <option value="'+ data[0]['estado'] + '" selected>' + data[0]['estado'] + '</option>\
                            <option value="Acre">Acre</option>\
                            <option value="Alagoas">Alagoas</option>\
                            <option value="Amapá">Amapá</option>\
                            <option value="Amazonas">Amazonas</option>\
                            <option value="Bahia">Bahia</option>\
                            <option value="Ceará">Ceará</option>\
                            <option value="Distrito Federal">Distrito Federal</option>\
                            <option value="Espírito Santo">Espírito Santo</option>\
                            <option value="Goiás">Goiás</option>\
                            <option value="Maranhão">Maranhão</option>\
                            <option value="Mato Grosso">Mato Grosso</option>\
                            <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>\
                            <option value="Minas Gerais">Minas Gerais</option>\
                            <option value="Pará">Pará</option>\
                            <option value="Paraíba">Paraíba</option>\
                            <option value="Paraná">Paraná</option>\
                            <option value="Pernambuco">Pernambuco</option>\
                            <option value="Piauí">Piauí</option>\
                            <option value="Rio de Janeiro">Rio de Janeiro</option>\
                            <option value="Rio Grande do Norte">Rio Grande do Norte</option>\
                            <option value="Rio Grande do Sul">Rio Grande do Sul</option>\
                            <option value="RO">Rondônia</option>\
                            <option value="Rondônia">Roraima</option>\
                            <option value="Santa Catarina">Santa Catarina</option>\
                            <option value="São Paulo">São Paulo</option>\
                            <option value="Sergipe">Sergipe</option>\
                            <option value="Tocantins">Tocantins</option>\
                            <option value="Estrangeiro">Estrangeiro</option>\
                        </select>\
                      </div>\
                      <div class="form-group">\
                      <label for="observacao">Observação</label>\
                      <textarea class="form-control" id="obs" name="obs" rows="3">'+ obs + '</textarea>\
                    </div>\
                      <div id="func-edit">\
                        <div class="form-group" id="select-prop" >\
                          <label for="func">Proprietário</label>\
                          <select readonly="readonly" class="form-control select2" id="func" name="func">\
                                <option id="prop" value="'+ data[0]['func_id'] + '" selected>' + data[0]['nome'] + '</option>\
                          </select>\
                        </div>\
                        <div class="form-group">\
                          <a class="btn btn-danger text-white" id="button-edit-desv" onclick="desvprop()"> Desvincular proprietário </a>\
                          <a class="btn btn-info text-white" id="button-edit-alt" onclick="altprop()"> Alterar proprietário </a>\
                        </div>\
                        <div class="form-group" >\
                          <label for="estado">Status da Linha</label>\
                          <select readonly="readonly" class="form-control" id="status" name="status">\
                                <option value="'+ data[0]['status_linha'] + '" selected>' + status_linha + '</option>\
                                <option value="0">Disponível</option>\
                                <option value="1">Ocupado</option>\
                          </select>\
                        </div>\
                      </div>';

      }
      else {

        var status_linha = "";
        var obs = "";

        if (data.status_linha == 0) {
          var status_linha = "Disponível";
        }
        if (data.status_linha == 1) {
          var status_linha = "Ocupada";
        }

        var numero = data.num_linha;
        var estado = data.estado;



        if (!!data.observacao) {
          var obs = data.observacao;
        }

        div.innerHTML = '<div class="form-group">\
                    <input type="hidden" id="id" value="'+ data.id + '">\
                    </div>\
                    <div class="form-group">\
                      <label for="estado">Número do telefone</label>\
                      <input type="text" class="form-control telefone" name="tel" id="tel" aria-describedby="numeroFone" value="'+ numero + '">\
                     </div>\
                     <div class="form-group">\
                      <label for="estado">Estado</label>\
                      <select class="form-control select2bs4 select2bs4-danger" style="width: 100%;" id="est" name="est">\
                            <option value="'+ estado + '" selected>' + estado + '</option>\
                            <option value="Acre">Acre</option>\
                            <option value="Alagoas">Alagoas</option>\
                            <option value="Amapá">Amapá</option>\
                            <option value="Amazonas">Amazonas</option>\
                            <option value="Bahia">Bahia</option>\
                            <option value="Ceará">Ceará</option>\
                            <option value="Distrito Federal">Distrito Federal</option>\
                            <option value="Espírito Santo">Espírito Santo</option>\
                            <option value="Goiás">Goiás</option>\
                            <option value="Maranhão">Maranhão</option>\
                            <option value="Mato Grosso">Mato Grosso</option>\
                            <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>\
                            <option value="Minas Gerais">Minas Gerais</option>\
                            <option value="Pará">Pará</option>\
                            <option value="Paraíba">Paraíba</option>\
                            <option value="Paraná">Paraná</option>\
                            <option value="Pernambuco">Pernambuco</option>\
                            <option value="Piauí">Piauí</option>\
                            <option value="Rio de Janeiro">Rio de Janeiro</option>\
                            <option value="Rio Grande do Norte">Rio Grande do Norte</option>\
                            <option value="Rio Grande do Sul">Rio Grande do Sul</option>\
                            <option value="RO">Rondônia</option>\
                            <option value="Rondônia">Roraima</option>\
                            <option value="Santa Catarina">Santa Catarina</option>\
                            <option value="São Paulo">São Paulo</option>\
                            <option value="Sergipe">Sergipe</option>\
                            <option value="Tocantins">Tocantins</option>\
                            <option value="Estrangeiro">Estrangeiro</option>\
                        </select>\
                      </div>\
                      <div class="form-group">\
                      <label for="observacao">Observação</label>\
                      <textarea class="form-control" id="obs" name="obs" rows="3">'+ obs + '</textarea>\
                    </div>\
                    <div id="func-edit">\
                        <div class="form-group" id="select-prop" >\
                        </div>\
                    <div class="form-group" id="btn-vinc">\
                      <a class="btn btn-info text-white" id="button-edit-alt" onclick="altprop()"> Vincular proprietário </a>\
                    </div>';

      }

      $("#modalEdit").modal();


      // console.log(data);


    }

  });


}

//função que abre o modal de deletar número da linha
function deletar(id) {
  // console.log(id);

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })

  swalWithBootstrapButtons.fire({
    title: 'Atenção!!',
    text: "Deseja realmente excluir definitivamente a linha?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Sim, excuir! ',
    cancelButtonText: 'Não, cancelar!',
    reverseButtons: true
  }).then((result) => {
    if (result.value) {


      var id_linha = id;

      $.ajax({

        type: 'post',

        url: '/admin/linhas/deleta_linha',

        data: { id_linha: id_linha },

        success: function (data) {

          console.log(data.success);
          // $('#linhasDisp').DataTable().ajax.reload();

          // toastr.success('Linha alterada com sucesso!');


          var x = document.getElementById("func-edit");

          x.innerHTML = '';

          $('#table_disp').DataTable().ajax.reload();
          $('#table_ocup').DataTable().ajax.reload();
          $("#editarL").modal('hide');
          reload_ind();
          toastr.success('Linha deletada com sucesso!');

        }
      });





    } else if (
      /* Read more about handling dismissals below */
      result.dismiss === Swal.DismissReason.cancel
    ) { }
  });


}


// Função que salva a linha ao clicar no botão "Cadastrar"
$("#salvarLinha").click(function (e) {

  e.preventDefault();

  var telefone = $("input[name=telefone]").val();

  var estado = $("#estado").val();

  var nome = $("#nome").val();

  var observacao = $("#observacao").val();

  $.ajax({

    type: 'POST',

    url: '/admin/linhas/salvar_linha',

    data: { telefone: telefone, estado: estado, nome: nome, observacao: observacao },

    success: function (data) {

      console.log(data.success);
      $('#table_disp').DataTable().ajax.reload();
      $('#table_ocup').DataTable().ajax.reload();
      $("#cadLinha").modal('hide');
      reload_ind();
      toastr.success('Linha cadastrada com sucesso!');

    }
  });
});

// Funcção salva a edição ao clicar no botão "Salvar Edição"
$("#editarLinha").click(function (e) {
  e.preventDefault();

  var id = $("#id").val();
  var id_linha = $("#linha_id").val();
  var id_func = $("#id_func").val();
  var telefone = $("input[name=tel]").val();
  var estado = $("#est").val();
  var prop = $("#prop").val();
  var observacao = $("#obs").val();

  console.log('clicado');
  console.log('Número: ' + telefone + ' - id: ' + id + ' - Estado: ' + estado + ' - Observação: ' + observacao + ' - ID linha: ' + id_linha + ' - Id func: ' + id_func + ' - prop: ' + prop);
  $.ajax({

    type: 'POST',

    url: '/admin/linhas/editar_linha',

    data: { id: id, id_linha: id_linha, id_func: id_func, telefone: telefone, estado: estado, prop: prop, observacao: observacao },

    success: function (data) {

      console.log(data.success);
      $('#table_disp').DataTable().ajax.reload();
      $('#table_ocup').DataTable().ajax.reload();
      $("#editarL").modal('hide');
      reload_ind();
      toastr.success('Linha alterada com sucesso!');
    }
  });
});

// // Função que deleta a linha
// $("#deletarLinha").click(function (e) {
//   e.preventDefault();
//   var id_linha = $("#linha_id").val();

//   console.log(id_linha);

// });


