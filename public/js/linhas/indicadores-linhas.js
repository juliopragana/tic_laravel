$(function () {

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });


    function carrega_ind() {
        $.ajax({

            type: 'get',

            url: '/admin/linhas/get_indic',

            success: function (data) {

                var totalL = document.getElementById("totalL");
                totalL.innerHTML = data['total'];
                var totalD = document.getElementById("totalD");
                totalD.innerHTML = data['linhas_d'];
                var totalO = document.getElementById("totalO");
                totalO.innerHTML = data['linhas_o'];
                var totalP = document.getElementById("totalP");
                totalP.innerHTML = data['linhas_p'];
                console.log(data);
            }

        });
    }



    carrega_ind();

})

function reload_ind() {
    $.ajax({

        type: 'get',

        url: '/admin/linhas/get_indic',

        success: function (data) {

            var totalL = document.getElementById("totalL");
            totalL.innerHTML = data['total'];
            var totalD = document.getElementById("totalD");
            totalD.innerHTML = data['linhas_d'];
            var totalO = document.getElementById("totalO");
            totalO.innerHTML = data['linhas_o'];
            var totalP = document.getElementById("totalP");
            totalP.innerHTML = data['linhas_p'];
            console.log(data);
        }

    });
}