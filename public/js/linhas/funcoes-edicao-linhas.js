

//   function test(id){
//     console.log(id);
//   }
function desvprop() {

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });


    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Atenção!!',
        text: "Deseja descvincular o proprietário da linha? A linha se tornará disponível!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sim, desvincular! ',
        cancelButtonText: 'Não, cancelar!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {


            var id_linha = $("#linha_id").val();

            $.ajax({

                type: 'post',

                url: '/admin/linhas/desv_linha',

                data: { id_linha: id_linha },

                success: function (data) {

                    console.log(data.success);
                    // $('#linhasDisp').DataTable().ajax.reload();

                    // toastr.success('Linha alterada com sucesso!');


                    var x = document.getElementById("func-edit");

                    x.innerHTML = '';
                    toastr.success('Alteração realizada com sucesso! A linha foi desvinculada e encontra-se disponível');
                    $('#table_disp').DataTable().ajax.reload();
                    $('#table_ocup').DataTable().ajax.reload();
                    reload_ind();
                    $("#editarL").modal('hide');
                }
            });





        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) { }
    });

}

function altprop() {

    $.ajax({

        type: 'post',

        url: '/admin/funcionarios',

        success: function (data) {

            var select = document.getElementById("select-prop");

            select.innerHTML = '<div class="form-group" id="select-prop" >\
                                <label for="func">Novo Proprietário</label>\
                                    <select class="form-control select2bs4 select2bs4-danger" id="prop" name="prop">\
                                    </select>\
                            </div>';

            data.forEach(function (item) {

                $('#prop').append('<option value="' + item.id + '">' + item.nome + '</option>');
            });

            // document.getElementById('btn-vinc').style.visibility='hidden';
            document.getElementById("btn-vinc").innerHTML = "";




            // console.log(data);
            // $('#linhasDisp').DataTable().ajax.reload();
        }
    });



    // var x = document.getElementById("prop");
    // x.innerHTML = '<p>Teste</p>';
}





