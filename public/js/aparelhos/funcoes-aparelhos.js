$(function () {

    //função que oculta ou exibe o funcionário no cadastro da linha
  $('[name="check"]').change(function () {
    $('#minhaDiv').toggle(100);
  });


  //Select2
  $('.select2').select2()
  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

  // Função do CSRF - Larável
  $.ajaxSetup({

    headers: {

      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });


  //função que carrega a DataTable de linhas disponíveis
  var table = $('#table_aparelhos').DataTable({
    "ajax": {
      "url": "/admin/aparelhos/carrega_tabela_aparelhos",
      "dataSrc": ""
    },
    "bProcessing": true,
    "columns": [

      { "data": "num_linha", "className": "text-center" },
      { "data": "estado", "className": "text-center" },
      { "data": "st_linha", "className": "text-center" },
      { "data": "observacao", "className": "text-center" },
      {
        "data": "id", "className": "text-center", render: function (data, type, row, meta) {
          return type === 'display' ?
            '<button class="btn btn-info btn-sm visualizar" onclick="vizualizar(' + data + ')" data-id="' + data + '"><i class="fas fa-eye"></i></button>\
                  <button class="btn btn-warning btn-sm modalEdicao" onclick="editar('+ data + ')" data-id="' + data + '"><i class="fas fa-edit"></i></button>\
                  <button class="btn btn-danger btn-sm" onclick="deletar('+ data + ')" data-id="' + data + '"><i class="fas fa-trash-alt"></i></button>\
                  '
            :
            data;
        }
      },
    ],
    "language": {
      "url": "/vendor/datatables/dataTables-pt_br.json"
    }
  });

})