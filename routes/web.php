<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {


    


    Route::namespace('Admin')->group(function () {

        Route::prefix('admin')->group( function(){
            // Home
            Route::get('/', 'AdminController@index')->name('admin.home');

            // ################  Emprestimos
            Route::get('emprestimos', 'EmprestimoController@index');

            // ################  Linhas


            //Rota principal das linhas
            Route::get('linhas', 'LinhaController@index');
            //Rota que carrega os dados dos indicadores
            Route::get('linhas/get_indic', 'LinhaController@get_ind');
            //rota que carrega o datable das linhas disponíveis
            Route::get('linhas/carrega_tabela_disp', 'LinhaController@carregaTablesdisp');
            //rota que carrega o datable das linhas ocupadas
            Route::get('linhas/carrega_tabela_ocup', 'LinhaController@carregaTablesocup');
            //Rota que salva a linha
            Route::post('linhas/salvar_linha', 'LinhaController@salvar');
            //Rota que carrega as informações da linha
            Route::post('linhas/infor_linha', 'LinhaController@info');
            //Rota que enviar dados para o modal de edição
            Route::post('linhas/modal_edit', 'LinhaController@modal_edit');
            //Rota que edita a linha
            Route::post('linhas/editar_linha', 'LinhaController@editar');
            //Rota que faz a desvinculação da linha
            Route::post('linhas/desv_linha', 'LinhaController@desv_linha');
            //Rota que faz a exclusão da linha
            Route::post('linhas/deleta_linha', 'LinhaController@deleta_linha');

            // ################  Aparelhos

            //Rota principal de aparelhos
            Route::get('aparelhos', 'AparelhoController@index');
            //Rota que carrega os dados dos indicadores
            Route::get('aparelhos/get_indic', 'LinhaController@get_ind');
            


            // ###############  Funcionários

            //Rota que lista funcionários
            Route::post('funcionarios', 'FuncionarioController@funcionarios');
            //Rota que carrega a tabela de Aparelhos


            // Teste
            Route::get('debug',  'LinhaController@debug');

        });
       
    });

    
});


Route::get('/', 'Site\SiteController@index')->name('home');
 
Auth::routes();