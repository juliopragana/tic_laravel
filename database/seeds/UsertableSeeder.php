<?php

use Illuminate\Database\Seeder;
use App\User;

class UsertableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Julio Pragana',
            'email' => 'julinhopragana@gmail.com',
            'password'=> bcrypt('123456')
        ]);
    }
}
