<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmprestimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aparelho_id')->unsigned();
            $table->foreign('aparelho_id')->references('id')->on('aparelhos')->onDelete('cascade');
            $table->string('num_chamado')->nullabre();
            $table->string('modelo_antigo')->nullabre();
            $table->longText('obs')->nullabre();
            $table->string('data_dev')->nullabre();
            $table->integer('termo_resp');
            $table->integer('termo_dev');
            $table->integer('funcionario_id')->unsigned();
            $table->foreign('funcionario_id')->references('id')->on('funcionarios')->onDelete('cascade');
            $table->integer('status_emp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimos');
    }
}
