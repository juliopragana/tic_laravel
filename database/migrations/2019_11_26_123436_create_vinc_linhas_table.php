<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVincLinhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vinc_linhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('func_id')->unsigned();
            $table->foreign('func_id')->references('id')->on('funcionarios')->onDelete('cascade');
            $table->integer('linha_id')->unsigned();
            $table->foreign('linha_id')->references('id')->on('linhas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vinc_linhas');
    }
}
