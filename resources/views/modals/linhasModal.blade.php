<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Modal -->
<style>
  .invisivel {
		display: none;
	}
</style>

<!-- Modal para cadastrar linha -->

      <div class="modal fade" id="cadLinha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cadastrar linha</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form>
            @csrf
                <div class="form-group">
                  <label for="estado">Número do telefone</label>
                  <input type="text" class="form-control telefone" name="telefone" id="telefone" aria-describedby="numeroFone" placeholder="Exemplo (99) 99999-9999">
                </div>
            <div class="form-group">
                  <label for="estado">Estado</label>
                  <select class="form-control select2bs4 select2bs4-danger" id="estado" name="estado">
                        <option value="">Selecione o estado</option>
                        <option value="Acre">Acre</option>
                        <option value="Alagoas">Alagoas</option>
                        <option value="Amapá">Amapá</option>
                        <option value="Amazonas">Amazonas</option>
                        <option value="Bahia">Bahia</option>
                        <option value="Ceará">Ceará</option>
                        <option value="Distrito Federal">Distrito Federal</option>
                        <option value="Espírito Santo">Espírito Santo</option>
                        <option value="Goiás">Goiás</option>
                        <option value="Maranhão">Maranhão</option>
                        <option value="Mato Grosso">Mato Grosso</option>
                        <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                        <option value="Minas Gerais">Minas Gerais</option>
                        <option value="Pará">Pará</option>
                        <option value="Paraíba">Paraíba</option>
                        <option value="Paraná">Paraná</option>
                        <option value="Pernambuco">Pernambuco</option>
                        <option value="Piauí">Piauí</option>
                        <option value="Rio de Janeiro">Rio de Janeiro</option>
                        <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                        <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                        <option value="RO">Rondônia</option>
                        <option value="Rondônia">Roraima</option>
                        <option value="Santa Catarina">Santa Catarina</option>
                        <option value="São Paulo">São Paulo</option>
                        <option value="Sergipe">Sergipe</option>
                        <option value="Tocantins">Tocantins</option>
                        <option value="Estrangeiro">Estrangeiro</option>
                    </select>
                </div>
                <div class="form-group">
                  <label for="observacao">Observação</label>
                  <textarea class="form-control" id="observacao" name="observacao" rows="3"></textarea>
                </div>
                <div class="form-group form-check">
                  <input type="checkbox" name="check" class="form-check-input" id="check">
                  <label class="form-check-label" for="exampleCheck1">Vincular contato?</label>
                </div>
                <div id="minhaDiv" class='invisivel'>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Nome do colaborador a ser vinculado</label>
                        <select class="form-control select2bs4 select2bs4-danger" style="width: 100%;" id="nome" name="nome">
                          <option value="">Selecione o funcionário</option>
                          @foreach ($funcs as $func)
                            <option value="{{$func->id}}">{{$func->nome}}</option>
                          @endforeach 
                          
                        </select>
                      </div>
                </div>
                                          
            </div>
            <div class="modal-footer">
            <button class="btn btn-primary" id="salvarLinha" >Cadastrar</button>
            </form> 
            </div>
          </div>
        </div>
      </div>
      

    <!-- Modal de vinculação -->
      <div class="modal fade" id="vincLinha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Vincular linha</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="vincLinha" >
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Linha:</label>
                                <select class="form-control" id="telefone">
                                  <option value="">Selecione a linha</option>
                                  @foreach ($linhasD as $linhas)
                                
                                <option value="{{$linhas->id}}">{{$linhas->num_linha}}  |  {{$linhas->estado}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>
                                                             
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Operador:</label>
                                    <select class="form-control">
                                      <option value="">Selecione o funcionário</option>
                                      @foreach ($funcs as $func)
                                     <option value="{{$func->id}}">{{$func->nome}}</option>
                                      @endforeach
                                      
                                    </select>
                                </div>
                            
                            
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Vincular</button>
            </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal de inforamções -->
      <div class="modal fade" id="infoLinha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-dark">
                <h5 class="modal-title" id="exampleModalLabel">Informações sobre a linha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="divInfoLinhas">
              
                  
                              
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>

      <!---------------------- Modal de edição --------------------->

      <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdicao" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header bg-dark">
                <h5 class="modal-title" id="exampleModalLabel">Editar Linha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <form>
                    @csrf
                  <div class="modal-body" id="editarL">
                      
                  </div>
                  
                
              <div class="modal-footer">
                  <button class="btn btn-primary" id="editarLinha" >Salvar edição</button>
                  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Fechar">
                  Fechar
                </button>
              </div>
              
              </form> 

            </div>
          </div>
      </div>

