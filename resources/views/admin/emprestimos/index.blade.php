{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Empréstimos</h1>
@stop

@section('content')
<!-- Main content -->
      <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>216</h3>

                <p>Total de Empréstimos</p>
              </div>
              <div class="icon">
                <i class="fas fa-sync"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53</h3>

                <p>Empréstimos OK</p>
              </div>
              <div class="icon">
                <i class="fas fa-sync"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>8</h3>

                <p>Empréstimos com atenção</p>
              </div>
              <div class="icon">
                <i class="fas fa-sync"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>1</h3>

                <p>Empréstimos pendentes</p>
              </div>
              <div class="icon">
                <i class="fas fa-sync"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
        </div>
<div class="card">
<div class="card-header bg-dark">
  <h3 class="card-title mt-2 p-0">Lista de empréstimos</h3>
  <button type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i> Adicionar Empréstimo</button>
</div>
<!-- /.card-header -->
<div class="card-body">
  <table id="example" class="table table-hover table-sm">
    <thead>
    <tr>
      <th>Nome do colaborador</th>
      <th>Telefone</th>
      <th>Aparelho</th>
      <th>Unidade</th>
      <th>Observações</th>
      <th>Ação</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>Trident</td>
      <td>Internet
        Explorer 4.0
      </td>
      <td>Win 95+</td>
      <td> 4</td>
      <td>X</td>
      <td></td>
    </tr>
    <tr>
      <td>Trident</td>
      <td>Internet
        Explorer 5.0
      </td>
      <td>Win 95+</td>
      <td>5</td>
      <td>C</td>
      <td></td>
    </tr>
    <tr>
      <td>Trident</td>
      <td>Internet
        Explorer 5.5
      </td>
      <td>Win 95+</td>
      <td>5.5</td>
      <td>A</td>
      <td></td>
    </tr>
    <tr>
      <td>Trident</td>
      <td>Internet
        Explorer 6
      </td>
      <td>Win 98+</td>
      <td>6</td>
      <td>A</td>
      <td></td>
    </tr>
    
    </tbody>
   
  </table>
</div>

@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<script>
        $(function () {
                // $("#table-aviso").DataTable();
                $('#example').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                    },
                    "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
                    stateSave: true
                });
        });
</script>
    <script> console.log('Hi!'); </script>
@stop