{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<!-- Main content -->
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title mb-0">Empréstimos</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body p-0 mb-0 ml-2 mr-2">
            <div class="row mt-3">
          
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>216</h3>
      
                      <p>Total de Empréstimos</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-sync"></i>
                    </div>
                    
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>53</h3>
      
                      <p>Empréstimos OK</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-sync"></i>
                    </div>
                    
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-warning">
                    <div class="inner">
                      <h3>8</h3>
      
                      <p>Empréstimos com atenção</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-sync"></i>
                    </div>
                    
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h3>1</h3>
      
                      <p>Empréstimos pendentes</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-sync"></i>
                    </div>
                    
                  </div>
                </div>
              </div> 

        </div>
       
        <!-- /.card-footer-->
      </div>   

      <div class="card">
          <div class="card-header">
            <h3 class="card-title mb-0">Número de linhas</h3>
  
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body p-0 mb-0 ml-2 mr-2">
              <div class="row mt-3">
            
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                      <div class="inner">
                        <h3>216</h3>
        
                        <p>Total de Linhas</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-phone-square-alt"></i>
                      </div>
                      
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3>53</h3>
        
                        <p>Linhas OK</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-phone-square-alt"></i>
                      </div>
                      
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                      <div class="inner">
                        <h3>8</h3>
        
                        <p>Linhas com atenção</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-phone-square-alt"></i>
                      </div>
                      
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                      <div class="inner">
                        <h3>1</h3>
        
                        <p>Linhas pendentes</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-phone-square-alt"></i>
                      </div>
                      
                    </div>
                  </div>
                </div> 
  
          </div>
         
          <!-- /.card-footer-->
        </div>  
        <div class="card">
            <div class="card-header">
              <h3 class="card-title mb-0">Número de Aparelhos</h3>
    
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body p-0 mb-0 ml-2 mr-2">
                <div class="row mt-3">
              
                    <div class="col-lg-3 col-6">
                      <!-- small box -->
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h3>216</h3>
          
                          <p>Total de aparelhos</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-mobile-alt"></i>
                        </div>
                        
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                      <!-- small box -->
                      <div class="small-box bg-success">
                        <div class="inner">
                          <h3>53</h3>
          
                          <p>Aparelhos OK</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-mobile-alt"></i>
                        </div>
                        
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                      <!-- small box -->
                      <div class="small-box bg-warning">
                        <div class="inner">
                          <h3>8</h3>
          
                          <p>Aparelhos com atenção</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-mobile-alt"></i>
                        </div>
                        
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                      <!-- small box -->
                      <div class="small-box bg-danger">
                        <div class="inner">
                          <h3>1</h3>
          
                          <p>Aparelhos pendentes</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-mobile-alt"></i>
                        </div>
                        
                      </div>
                    </div>
                  </div> 
    
            </div>
           
            <!-- /.card-footer-->
          </div>   
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@stop

@section('js')

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>


<script>

        $(function () {
                // $("#table-aviso").DataTable();
                $('#example').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                    },
                    "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
                    stateSave: true
                });
        });

        
</script>
@stop

