
{{-- resources/views/admin/dashboard.blade.php --}}


@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Aparelhos</h1>
@stop

@section('content')
<script src="{{ asset('js/linhas/funcoes-edicao-linhas.js') }}"></script>
<style>

    .dados{
      color:brown;
    }
  
    .info{
      color: red;
    }
    select[readonly] {
      background: #eee; /*Simular campo inativo - Sugestão @GabrielRodrigues*/
      pointer-events: none;
      touch-action: none;
    }
  </style>

 <!-- Main content -->
         <!-- Small boxes (Stat box) -->
         <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                <h3 id='totalL'>0</h3>
  
                  <p>Total de Linhas</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              
              <div class="small-box bg-success">
                <div class="inner">
                  <h3 id='totalD'>0</h3>
  
                  <p>Linhas disponíveis</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3 id='totalO'>0</h3>
  
                  <p>Linhas em uso</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3 id='totalP'>0</h3>
  
                  <p>Linhas com Problemas</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
          </div>
          <div class="row">
              <div class="col-lg-12">
                  <div class="card">
                      <div class="card-header bg-dark">
                        
                        <h3 class="card-title mt-2 p-0">Lista de linhas</h3>
                        <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#cadLinha"><i class="fas fa-plus"></i> Cadastrar linha</button>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body">
                        
                            <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                              <li class="nav-item">
                                 <a class="nav-link active" id="linhas-disponiveis-tab" data-toggle="pill" href="#linhas-disponiveis" role="tab" aria-controls="linhas-disponiveis" aria-selected="true">Linhas disponíveis</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="linhas-ocupadas-tab" data-toggle="pill" href="#linhas-ocupadas" role="tab" aria-controls="linhas-ocupadas" aria-selected="false">Linhas em uso</a>
                              </li>
                                  
                            </ul>  

                            <div class="tab-content my-2" id="custom-content-below-tabContent">
                            <!-- Linhas Disponíveis -->
                                <div class="tab-pane fade show active" id="linhas-disponiveis" role="tabpanel" aria-labelledby="linhas-disponiveis-tab">
                                      <h3 class="p-1 text-center text-success">Linhas Disponíveis</h3>
                                      

                                      <table id="table_disp" class="table table-striped table-hover table-sm" width="100%">
                                      <thead>
                                        <tr>
                                            <th>Número linha</th>
                                            <th>Estado</th>
                                            <th>Status</th>
                                            <th>Observação</th>
                                            <th>Ação</th>
                                       </tr>
                                      </thead>
                                      <tbody>
                                        <!-- <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            
                                            
                                        </tr> -->
                                      </tbody>
                                      </table>

                                     
                                </div>
                                <div class="tab-pane fade" id="linhas-ocupadas" role="tabpanel" aria-labelledby="linhas-ocupadas-tab">
                                     <!-- Linhas em uso -->
                                  <h3 class="p-1 text-center text-danger">Linhas em uso</h3>
                                  
                                  <table id="table_ocup" class="table table-striped table-hover table-sm" width="100%">
                                      <thead>
                                        <tr>
                                            <th>Número linha</th>
                                            <th>Estado</th>
                                            <th>Status</th>
                                            <th>Observação</th>
                                            <th>Proprietário</th>
                                            <th>Ação</th>
                                       </tr>
                                      </thead>
                                      <tbody>
                                        <!-- <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            
                                            
                                        </tr> -->
                                      </tbody>
                                      </table>  


                                </div>  

                            </div>
                      </div>
                </div>
            </div>
          </div>
          @include('modals.linhasModal')  
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/linhas/indicadores-linhas.js') }}"></script>
<script src="{{ asset('js/linhas/dashboard-linhas.js') }}"></script>
<script src="{{ asset('js/linhas/funcoes-linhas.js') }}"></script>
<script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
  <!-- <script src="{{ asset('js/sweetalert2/dist/sweetalert2.all.min.js') }}"></script> -->
<script src="{{ asset('vendor/adminlte/dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/plugins/toastr/toastr.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


      
@stop