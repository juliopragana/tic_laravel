{{-- resources/views/admin/dashboard.blade.php --}}


@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Aparelhos</h1>
@stop

@section('content')
<style>

    .dados{
      color:brown;
    }
  
    .info{
      color: red;
    }
    select[readonly] {
      background: #eee; /*Simular campo inativo - Sugestão @GabrielRodrigues*/
      pointer-events: none;
      touch-action: none;
    }
  </style>
<!-- Main content -->
         <!-- Small boxes (Stat box) -->
         <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{$total}}</h3>
  
                  <p>Total de Linhas</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>{{$totalD}}</h3>
  
                  <p>Linhas disponíveis</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3>{{$totalU}}</h3>
  
                  <p>Linhas em uso</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>{{$totalP}}</h3>
  
                  <p>Linhas com Problemas</p>
                </div>
                <div class="icon">
                  <i class="fas fa-phone-square-alt"></i>
                </div>
                
              </div>
            </div>
            <!-- ./col -->
          </div>
          <div class="row">
            <div class="col-lg-6">
                <div class="card">
                <div class="card-header bg-dark">
                  
                  <h3 class="card-title mt-2 p-0">Lista de linhas</h3>
                  <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#cadLinha"><i class="fas fa-plus"></i> Cadastrar linha</button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                  <table id="linhas" class="table table-bordered table-hover table-sm" style="font-size: 14px; white-space: nowrap;" width='100%'>
                    <thead>
                    <tr>
                      <th style="display:none;"></th>
                      <th>Número da Linha</th>
                      <th>Estado</th>
                      <th>Status</th>
                      <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                          @if (!$linhas)
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          @endif
                          @foreach ($linhas as $linha)
                          <tr>
                            <td style="display:none;">{{$linha->id}}</td>
                            <td>{{$linha->num_linha}}</td>
                            <td>{{$linha->estado}}</td>
                            @if($linha->status_linha == 0)
                            <td style="color: green"> Disponível </td>
                            @endif
                            @if($linha->status_linha == 1)
                            <td style="color: red"> Ocupada </td>
                            @endif
                            <td>
                              <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    </button>
                                    <ul class="dropdown-menu">
                                      <li><button class="dropdown-item visualizar"  data-id="{{$linha->id}}">Visualizar</button></li>
                                      <li><button class="dropdown-item modalEdicao" data-id="{{$linha->id}}">Editar</button></li>
                                      <li><button class="dropdown-item" data-id="{{$linha->id}}">Excluir</button></li>
                                    </ul>
                                  </div>
                            </td>
                            
                          </tr>
                        @endforeach
                          
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                <div class="card-header bg-dark">
                  <h3 class="card-title mt-2 p-0">Lista de Linhas em uso</h3>
                  <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#vincLinha"><i class="fas fa-plus"></i> Vincular linha</button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="linhasuso" class="table table-bordered table-hover table-sm" style="font-size: 14px">
                    <thead>
                    <tr>
                      <th>Número</th>
                      <th>Nome</th>
                      <th>Status</th>
                      <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                          @if (!$linhasV)
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          @endif
                          @foreach ($linhasV as $linhav)
                          <tr>
                            <td style="white-space: nowrap;">{{$linhav->num_linha}}</td>
                            <td>{{$linhav->nome}}</td>
                            <td>{{$linhav->estado}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    </button>
                                    <ul class="dropdown-menu">
                                      <li><a class="dropdown-item" href="#">Visualizar</a></li>
                                      <li><a class="dropdown-item" href="#">Editar</a></li>
                                      <li><a class="dropdown-item" href="#">Excluir</a></li>
                                    </ul>
                                  </div>
  
  
                            </td>
                          </tr>
                          @endforeach
                      
                    </tbody>
                  
                  </table>
                  </div>
                </div>
            </div>
           
          </div>
          @include('modals.linhasModal')  
@stop


@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
<script src="{{ asset('js/funcoes.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

{{-- <script>
        $(function () {

          
          
          $.ajaxSetup({

              headers: {

                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

              }

          });


          $("#salvarLinha").click(function(e){

              e.preventDefault();

              var telefone = $("input[name=telefone]").val();

              var estado = $("#estado").val();
              
              var nome = $("#nome").val();

              $.ajax({

                  type:'POST',

                  url:'/admin/salvar_linha',

                  data:{telefone:telefone, estado:estado, nome:nome},

                  success:function(data){

                      console.log(data.success);
                  
                  }
              });
          });

          $(".visualizar").click(function(event){

              id = $(this).attr('data-id');
              console.log(id);
              $.ajax({

                type:'POST',

                url:'/admin/infor_linha',

                data:{id:id},

                success:function(data){
                                                  
                  var div = document.getElementById('divInfoLinhas');
                  
                  if(Array.isArray(data)){
                  
                    console.log(data[0]['nome']);
                    var status_linha = "";

                    if(data[0]['status_linha'] == 0){
                      var status_linha = "Disponível";
                    } 
                    if(data[0]['status_linha'] == 1){
                      var status_linha = "Ocupada";
                    }


                    div.innerHTML = 
                    "<h4>Dados da Linha</h4><br>\
                    <p> <b>Linha:</b> <span class='dados'>" + data[0]['num_linha'] + "</span></p>\
                    <p> <b>Estado:</b> <span class='dados'>" + data[0]['estado'] + "</span></p>\
                    <p> <b>Status:</b> <span class='dados'>" + status_linha + "</span></p><hr>\
                    <h4> Dados do proprietário </h4><br>\
                    <p> <b>Nome:</b> <span class='dados'>"+ data[0]['nome'] + "</span></p>\
                    <p> <b>Filial:</b> <span class='dados'>"+ data[0]['filial'] + "</span></p>\
                    <p> <b>Setor:</b> <span class='dados'>"+ data[0]['setor'] + "</span></p>\
                    <p> <b>Cargo:</b> <span class='dados'>"+ data[0]['cargo'] + "</span></p>\
                    <p> <b>Centro de Custo:</b> <span class='dados'>"+ data[0]['centro_custo'] + "</span></p>\
                    <p> <b>Número do C.C:</b> <span class='dados'>"+ data[0]['num_centro_custo'] + "</span></p>\
                    "; 
                    
                  }
                   else {

                    var status_linha = "";

                    if(data.status_linha == 0){
                      var status_linha = "Disponível";
                    } 
                    if(data.status_linha == 1){
                      var status_linha = "Ocupada";
                    }

                    var numero = data.num_linha;
                    var estado = data.estado;
                   

                    div.innerHTML = "<h5> Linha: " + numero + "</h5><h5> Estado: " + estado + "</h5> <h5> Status: " + status_linha + "</h5>"; 
                   
                   }
                  
                   $("#infoLinha").modal();
                  

                  console.log(data);
                  

                }

              });

          });

          $(".modalEdicao").click(function(event){
            id = $(this).attr('data-id');
              console.log(id);
              $.ajax({

                type:'POST',

                url:'/admin/modal_edit',

                data:{id:id},

                success:function(data){
                                                  
                  var div = document.getElementById('editarL');
                  
                  if(Array.isArray(data)){
                  
                    console.log(data[0]['nome']);
                    var status_linha = "";

                    if(data[0]['status_linha'] == 0){
                      var status_linha = "Disponível";
                    } 
                    if(data[0]['status_linha'] == 1){
                      var status_linha = "Ocupada";
                    }


                    div.innerHTML = 
                          '<div class="form-group">\
                            <label for="estado">Número do telefone</label>\
                            <input type="text" class="form-control telefone" name="telefone" id="telefone" aria-describedby="numeroFone" value="'+data[0]['num_linha']+'">\
                           </div>'
                    
                  }
                   else {

                    var status_linha = "";

                    if(data.status_linha == 0){
                      var status_linha = "Disponível";
                    } 
                    if(data.status_linha == 1){
                      var status_linha = "Ocupada";
                    }

                    var numero = data.num_linha;
                    var estado = data.estado;
                   

                    div.innerHTML = "<h5> Linha: " + numero + "</h5><h5> Estado: " + estado + "</h5> <h5> Status: " + status_linha + "</h5>"; 
                   
                   }
                  
                   $("#modalEdit").modal();
                  

                  console.log(data);
                  

                }

              });
              

          });


          


          
          $('[name="check"]').change(function() {
            $('#minhaDiv').toggle(100);
          });
          
          jQuery("input.telefone")
              .mask("(99) 9999-9999?9")
              .focusout(function (event) {  
                  var target, phone, element;  
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);  
                  element.unmask();  
                  if(phone.length > 10) {  
                      element.mask("(99) 99999-999?9");  
                  } else {  
                      element.mask("(99) 9999-9999?9");  
                  }  
            });
         
                // $("#table-aviso").DataTable();
            $('#linhas').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                    },
                    "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
                    stateSave: true
            });
            
            $('#linhasuso').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                    },
                    "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
                    stateSave: true
          });

        });

</script> --}}
    <script> console.log('Hi!'); </script>
@stop