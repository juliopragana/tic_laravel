$(function () {
 
          
    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });


    $("#salvarLinha").click(function(e){

        e.preventDefault();

        var telefone = $("input[name=telefone]").val();

        var estado = $("#estado").val();
        
        var nome = $("#nome").val();

        $.ajax({

            type:'POST',

            url:'/admin/salvar_linha',

            data:{telefone:telefone, estado:estado, nome:nome},

            success:function(data){

                console.log(data.success);
            
            }
        });
    });

    $(".visualizar").click(function(event){

        id = $(this).attr('data-id');
        console.log(id);
        $.ajax({

          type:'POST',

          url:'/admin/infor_linha',

          data:{id:id},

          success:function(data){
                                            
            var div = document.getElementById('divInfoLinhas');
            
            if(Array.isArray(data)){
            
              console.log(data[0]['nome']);
              var status_linha = "";

              if(data[0]['status_linha'] == 0){
                var status_linha = "Disponível";
              } 
              if(data[0]['status_linha'] == 1){
                var status_linha = "Ocupada";
              }


              div.innerHTML = 
              "<h4>Dados da Linha</h4><br>\
              <p> <b>Linha:</b> <span class='dados'>" + data[0]['num_linha'] + "</span></p>\
              <p> <b>Estado:</b> <span class='dados'>" + data[0]['estado'] + "</span></p>\
              <p> <b>Status:</b> <span class='dados'>" + status_linha + "</span></p><hr>\
              <p> <b>Observação:</b> <span class='dados'>" + data[0]['observacao'] + "</span></p><hr>\
              <h4> Dados do proprietário </h4><br>\
              <p> <b>Nome:</b> <span class='dados'>"+ data[0]['nome'] + "</span></p>\
              <p> <b>Filial:</b> <span class='dados'>"+ data[0]['filial'] + "</span></p>\
              <p> <b>Setor:</b> <span class='dados'>"+ data[0]['setor'] + "</span></p>\
              <p> <b>Cargo:</b> <span class='dados'>"+ data[0]['cargo'] + "</span></p>\
              <p> <b>Centro de Custo:</b> <span class='dados'>"+ data[0]['centro_custo'] + "</span></p>\
              <p> <b>Número do C.C:</b> <span class='dados'>"+ data[0]['num_centro_custo'] + "</span></p>\
              "; 
              
            }
             else {

              var status_linha = "";

              if(data.status_linha == 0){
                var status_linha = "Disponível";
              } 
              if(data.status_linha == 1){
                var status_linha = "Ocupada";
              }

              var numero = data.num_linha;
              var estado = data.estado;
              var observacao = data.observacao;

              div.innerHTML = "<h5> Linha: " + numero + "</h5><h5> Estado: " + estado + "</h5> <h5> Status: " + status_linha + "</h5>"; 
             
             }
            
             $("#infoLinha").modal();
            

            console.log(data);
            

          }

        });

    });

    $(".modalEdicao").click(function(event){
      id = $(this).attr('data-id');
        console.log(id);
        $.ajax({

          type:'POST',

          url:'/admin/modal_edit',

          data:{id:id},

          success:function(data){
                                            
            var div = document.getElementById('editarL');
            
            if(Array.isArray(data)){
            
              console.log(data[0]['nome']);
              var status_linha = "";

              if(data[0]['status_linha'] == 0){
                var status_linha = "Disponível";
              } 
              if(data[0]['status_linha'] == 1){
                var status_linha = "Ocupada";
              }


              div.innerHTML = 
                    '<div class="form-group">\
                      <label for="estado">Número do telefone</label>\
                      <input type="text" class="form-control telefone" name="telefone" id="telefone" aria-describedby="numeroFone" value="'+data[0]['num_linha']+'">\
                     </div>'
              
            }
             else {

              var status_linha = "";

              if(data.status_linha == 0){
                var status_linha = "Disponível";
              } 
              if(data.status_linha == 1){
                var status_linha = "Ocupada";
              }

              var numero = data.num_linha;
              var estado = data.estado;
             

              div.innerHTML = "<h5> Linha: " + numero + "</h5><h5> Estado: " + estado + "</h5> <h5> Status: " + status_linha + "</h5>"; 
             
             }
            
             $("#modalEdit").modal();
            

            console.log(data);
            

          }

        });
        

    });


    


    
    $('[name="check"]').change(function() {
      $('#minhaDiv').toggle(100);
    });
    
    jQuery("input.telefone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
      });
   
          // $("#table-aviso").DataTable();
      $('#linhas').DataTable({
              "language": {
                  "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
              },
              "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
              stateSave: true
      });
      
      $('#linhasuso').DataTable({
              "language": {
                  "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
              },
              "lengthMenu": [[1000, 10, 20, 50, -1], ["Todos", 10, 20, 50, 100]],
              stateSave: true
    });

  });